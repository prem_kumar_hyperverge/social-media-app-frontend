**Problem Statement:**

Develop a _**Social media application**_  with functionalities such as login, posts, friends connection, like, comment and profile. Develop using MERN stack.

**Objectives:**

* Creating user schema with users personal information, posts schema which includes such as posted by, liked user name, comments, commented user name.
* As a problem statement to develop in MERN, so I have used MongoDB for the database and stores these information.
* Using NodeJS created an express server and developed an API.
* Using react in frontend developed a good-looking UI/UX for clients.

**Our Solution:**

- [ ] An Application that retrieves posts from API and if some users added some posts it will update in DB and fetched back from API.
- [ ] Suggest only friends post to users.
- [ ] Users can edit their profiles.
- [ ] Users can also delete their posts.
- [ ] Users can like and comment on their posts and friends' posts too.

**Pacakges Used:**


- Axios
- Bootstrap
- SweetAlert
- Spinners
- Animated Heart
- Cors
- Express
- Moongoose
- Jsonwebtoken
- BcryptJs
- Dotenv

**Demo:**

https://www.loom.com/share/9c4a2495b011457cb9876fcff5dcd114

**Local Run:**

`git clone social-media-app-frontend`

`cd social-media-app-frontend`

`npm install`

`npm run`

Go to web browser open  http://localhost:3000

