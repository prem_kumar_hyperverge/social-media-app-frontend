import React from "react";
import { BrowserRouter as Router, Switch, Route, Link, Redirect } from "react-router-dom";
import LoginPage from "./components/Login/Loginpage.jsx";
import RegisterPage from "./components/Register/RegisterPage.jsx";
import HomePage from "./components/Home/HomePage.jsx";
import ProfilePage from "./components/ProfilePage/ProfilePage.jsx";
import PublicPage from "./components/PublicPage/PublicPage.jsx";
import MyPage from "./components/MyPage/MyPage.jsx";
import Loaders from "./components/Loader/Loaders.jsx";
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"
function App () {
    return (
      <Router >
        <Route exact path="/">
          <HomePage />
        </Route>
        <Route exact path="/login">
          <LoginPage />
        </Route>
        <Route exact path="/register">
          <RegisterPage />
        </Route>
        <Route exact path="/profile">
          <ProfilePage />
        </Route>
        <Route exact path="/public-posts">
          <PublicPage />
        </Route>
        <Route exact path="/my-posts">
          <MyPage />
        </Route>
      </Router>
    );
}
export default App;
