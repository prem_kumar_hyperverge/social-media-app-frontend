import React, { useState } from "react";
import { Col } from 'reactstrap';
import { Button } from 'semantic-ui-react';
import "./Follow.css";
function Follow(props) {
    function doWork() {props.call(props.data.userId)
    console.log(props.data.userId)
    }
    return (
        <Col xl={3} lg={3} md={4}>
            <div class="follow-div">
                <div class="card text-center"><img class="card-img-tops"  src={props.data.profileImage} alt="" />
                    <div class="card-body">
                        <h5 >{props.data.userName} <br />
                            <Button onClick={doWork} >+{' '}Follow</Button>
                        </h5>
                    </div>
                </div>
            </div>
        </Col>
    )
    
}

export default Follow;