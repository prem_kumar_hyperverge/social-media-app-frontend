import React, { useState, useEffect } from "react";
import FileBase64 from 'react-file-base64';
import { Button, Comment, Form } from 'semantic-ui-react';
import SweetAlert from 'react-bootstrap-sweetalert';
import Heart from "react-animated-heart";
import { NavLink, Redirect, useHistory } from "react-router-dom";
import { Col, FormGroup, Input, Label, Row, Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavbarText, Container, Modal, ModalHeader, ModalBody, ModalFooter, Badge } from 'reactstrap';
import Loaders from '../Loader/Loaders';
import Follow from '../Follow/Follow';
import Posts from '../Posts/Posts';
import axios from "axios";
import {user,post,token,userId,userName,profileImage,headers} from '../../constants/data'
import "./HomePage.css";
import logo from '../../assests/images/logo.png'

function HomePage() {
    let history = useHistory();
    const [isOpen, setIsopen] = useState(false);
    const [commentSectionView, setCommentSectionView] = useState(false);
    const [postsData, setPostsData] = useState([]);
    const [userComment, setUserComment] = useState('');
    const [userPostImage, setUserPostImage] = useState([]);
    const [userPostTitle, setUserPostTitle] = useState('');
    const [userPostDescription, setUserPostDescription] = useState('');
    const [modal, setModal] = useState(false);
    const [spinner, setSpinner] = useState(false);
    const [swal, setSwal] = useState(false);
    const [postsEmpty, setPostsEmpty] = useState(false);
    const [userProfileImage, setUserProfileImage] = useState([]);
    const [postsMode, setPostsMode] = useState(false);
    const [usersData, setUsersData] = useState([]);
    const [commentsID, setCommentsID] = useState('');
    useEffect(() => {
        window.scrollTo(0, 0);
        console.log(user,post);
        getDetails();
    }, []);
    const getDetails = async () => {
        setSpinner(true); setPostsEmpty(false);
        const data = {
            userId: userId
        }
        await axios.post(post + "friendsPosts", data, { headers })
            .then(async dataObj => {
                setPostsData(dataObj.data.data);
                await axios.post(user + "getUsers", data, { headers })
                    .then(async res => {
                        for (var i = 0; i < res.data.data.length; i++) {
                            if (res.data.data[i].userId == localStorage.getItem("userId")) {
                                res.data.data.splice(i, 1);
                            }
                        }
                        setUsersData(res.data.data); setPostsEmpty(false); setSpinner(false);
                    }).catch(err => {
                        setSpinner(false); setPostsEmpty(false); setSwal(true);
                    })
            }).catch(err => {
                setSpinner(false); setPostsEmpty(false); setSwal(true);
            })
    }
    const toggle = async () => {
        setIsopen(!isOpen);
    }
    const comment = async (postId) => {
        setSpinner(true); setPostsEmpty(false);
        if (userComment.length == 0) {
            setSwal(true);
        }
        else {
            var dt = new Date();
            var hours = dt.getHours();
            var AmOrPm = hours >= 12 ? 'pm' : 'am';
            hours = (hours % 12) || 12;
            var minutes = dt.getMinutes();
            var finalTime = hours + ":" + minutes + " " + AmOrPm;
            const data = {
                userId: userId, postId: postId, comments: userComment, userName: userName, commentedAt: finalTime, profileImage: profileImage
            }
            await axios.post(post + "addComment", data, { headers }
            ).then(res => {
                setSpinner(false); setPostsEmpty(false); getDetails();
            }).catch(err => {
                setSpinner(false); setPostsEmpty(false); setSwal(true);
            })
        }
    }
    const toggleChange = async (postId, value) => {
        setSpinner(true); setPostsEmpty(false);
        const data = {
            userId: userId, postId: postId
        }
        if (value === false) {
            await axios.post(post + "like", data, { headers }).then(res => {
                setSpinner(false); getDetails();
            }).catch(err => {
                setSpinner(false); setSwal(true);
            })
        }
        else {
            await axios.post(post + "unlike", data, { headers }
            ).then(res => {
                setSpinner(false); getDetails();
            }).catch(err => {
                setSpinner(false); setSwal(true);
            })
        }
    }
    const getFiles = async (files) => {
        setUserPostImage(files.base64);
    }
    const openModal = async () => {
        setModal(!modal);
    }
    const save = async () => {
        setSpinner(true); setModal(false); setPostsEmpty(false);
        const data = {
            userId: userId,
            userPostTitle: userPostTitle,
            userPostDescription: userPostDescription,
            userPostImage: userPostImage,
            userName: userName,
            private: postsMode
        }
        await axios.post(post + "uploadPost", data, { headers }).then(res => {
            setSpinner(false); getDetails();
        }).catch(err => {
            setSpinner(false); setSwal(true); getDetails();

        })
    }
    const check = async (e) => {
        await setPostsMode(e);
    }
    const follow = async (friendsId) => {
        console.log(friendsId);
        setSpinner(true); setPostsEmpty(false);
        const data = {
            userId: userId,
            friendsId: friendsId,
        }
        await axios.post(user + "follow", data, { headers }).then(res => {
            setSpinner(false); getDetails();
        }).catch(err => {
            setSpinner(false); setSwal(true);
        })
    }
    const viewComments = async (postId) => {
        console.log("called")
        console.log(postId)
        setCommentsID(postId); setCommentSectionView(!commentSectionView);
    }
    const logout = async () => {
        localStorage.clear();
        history.push('/login')
    }
    const change = async (text)=>{
// console.log(text);
setUserComment(text);
    }
    if (!token) {
        return <Redirect to="/login" />
    }
    else {
        return (
            <div>
                <Navbar color="light" light expand="md">
                    <img src={logo} class="nav-img"
                    />&nbsp;&nbsp;
        <NavbarBrand href="/">Social Media</NavbarBrand>
                    <NavbarToggler onClick={toggle} />
               
                    <Collapse isOpen={isOpen} navbar>
                        <Nav className="mr-auto" navbar >
                        </Nav>
                        <NavLink to="/public-posts" class="navbar-text"> Public Posts</NavLink>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br />
                        <NavLink to="/my-posts" class="navbar-text"> My Posts</NavLink>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br />
                        <NavbarText class="navbar-text" onClick={openModal}>Upload Post</NavbarText>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br />
                        <NavLink to="/profile" class="navbar-text"> Profile</NavLink>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br />
                        <NavbarText class="navbar-text" onClick={logout}>Logout</NavbarText>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br />
        </Collapse>
                </Navbar>
                     <SweetAlert
                        error
                        show={swal}
                        title="Something went wrong, Please logout and login again"
                        onConfirm={() => setSwal(false)}
                    >
                    </SweetAlert>
                <Container>
                    <Row>
                        <Modal isOpen={modal} toggle={openModal} size="lg">
                            <ModalHeader xl={8} lg={12} md={12} toggle={openModal}>Upload Post</ModalHeader>
                            <ModalBody>
                                <Form>
                                    <FormGroup row>
                                        <Label sm={4}>
                                            PostTitle
                                        </Label>
                                        <Col sm={8}>
                                            <Input
                                                type="text"
                                                placeholder="Post Title"
                                                value={userPostTitle}
                                                onChange={(event) => setUserPostTitle(event.target.value)}
                                            />
                                        </Col>
                                    </FormGroup>
                                    <FormGroup row>
                                        <Label sm={4}>
                                            Post Description
                                        </Label>
                                        <Col sm={8}>
                                            <Input
                                                type="textarea"
                                                placeholder="Post Description"
                                                value={userPostDescription}
                                                onChange={(event) => setUserPostDescription(event.target.value)}
                                            />
                                        </Col>
                                    </FormGroup>
                                    <FormGroup row>
                                        <Label sm={4}>Post Image</Label>
                                        <Col sm={7}>
                                            <FileBase64 id="profileImage" name="profileImage" required onDone={getFiles.bind(this)} />
                                        </Col>
                                    </FormGroup>
                                    <FormGroup row>
                                        <Label sm={5}>
                                        </Label>
                                        <Col sm={7}>
                                            <Input
                                                type="checkbox"
                                                value={postsMode}
                                                onChange={(e) => check(e.target.checked)} />Private Post
                                        </Col>
                                    </FormGroup>
                                </Form>
                            </ModalBody>
                            <ModalFooter>
                                <Button color='success' onClick={save}>Save{' '}</Button>
                                <Button color='secondary' onClick={openModal}>Cancel</Button>
                            </ModalFooter>
                        </Modal>
                        {spinner === true ?
                            <Loaders />
                            :
                            <Col xl={12} lg={12} md={12} >
                                <div class="gtco-testimonials">
                                    {usersData.length === 0 ? null :
                                    <span>
                                    <h2>Peoples</h2>
                                    <Row>
                                        {usersData.map((data, index) => (
                                            <Follow data={data} call={follow}/>
                                        ))}
                                    </Row>
                                    </span>
                                        }
                                    <span>
                                        {postsData.length === 0 ?
                                            <p class="post-data"> No Post found</p>
                                            : <span>
                                                <h2>Posts</h2>
                                                
                                                        {postsData.map((data, index) => (
                                                            <Posts data={data} like={toggleChange} viewComment={viewComments} comment={comment} cId={commentsID} sectionView={commentSectionView} change={change}
                                                            
                                                            />
                                                        ))}
                                            </span>
                                        }
                                    </span>
                                </div>
                            </Col>
                        }
                    </Row>
                </Container>
            </div>
        )
    }
}

export default HomePage;
