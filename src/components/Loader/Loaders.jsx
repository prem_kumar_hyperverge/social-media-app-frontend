import React, { useState } from "react";
import Loader from 'react-loader-spinner'
function Loaders(props) {
  return (
    <Loader
      type="ThreeDots"
      color="#004E8E"
      height={50}
      width={50}
      style={{
        position: 'absolute', left: '50%', top: '50%',
        transform: 'translate(-50%, -50%)'
      }}
    />
  )
}
export default Loaders;