import React, { useState } from "react";
import { BrowserRouter, Link, Redirect, useHistory } from "react-router-dom";
import axios from 'axios';
import SweetAlert from 'react-bootstrap-sweetalert';
import { Button, CardBody, Col, FormGroup, Input, Label, Row, Container } from 'reactstrap';
import Loaders from '../Loader/Loaders.jsx';
import "./login.css";
import {user} from '../../constants/data'


function Loginpage() {
  let history = useHistory();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [spinner, setSpinner] = useState(false);
  const [swal, setSwal] = useState(false);

  const login = async () => {
    localStorage.clear();
    setSpinner(true);
    await axios.post(user + 'login', {
      email: email,
      password: password,
    }).then(response => {
      console.log(response)
      if (response.data.message === "Login success :)") {
        localStorage.setItem("token_social_media", response.data.token);
        localStorage.setItem("userId", response.data.data.user.id);
        localStorage.setItem("userName", response.data.data.user.userName);
        localStorage.setItem("profileImage", response.data.data.user.profileImage);
      }
      setSpinner(false);
      window.location.href="/";
     
    }).catch(err => {
      setSpinner(false); setSwal(true);
    })
  }
  return (
    <div>
      <Container>
        {spinner === true ?
          <Loaders />
          :
          <Row>
            <Col xl={5} lg={8} md={8} style={{
              position: 'absolute', left: '50%', top: '50%',
              transform: 'translate(-50%, -50%)', boxShadow: '0px 0px 15px rgba(0,0,0,0.15)'
            }}>
              <center>
                <h3 style={{ marginTop: '2%' }}>Sign In</h3></center>
              <CardBody>
                <FormGroup>
                  <Label >Email</Label>
                  <Input type="email" placeholder="john@gmail.com" onChange={(event) => setEmail(event.target.value)} style={{ backgroundColor: '#F0F5FB' }} />
                </FormGroup>
                <FormGroup>
                  <Label >Password</Label>
                  <Input type="password" placeholder="password" onChange={(event) => setPassword(event.target.value)} style={{ backgroundColor: '#F0F5FB' }} />
                </FormGroup>
                <center> <Button color="success" onClick={login}>Login</Button></center>
                <p className="text-right">
                  New User  <Link to='/register' >Register </Link>
                </p>
              </CardBody>
            </Col>
            <SweetAlert
              error
              show={swal}
              title="Please enter valid credentials"
              onConfirm={() => setSwal(false)}
            >
            </SweetAlert>
          </Row>
        }
      </Container>
    </div>
  )
};

export default Loginpage;
