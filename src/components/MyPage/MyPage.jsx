import React, { useState, useEffect} from "react";
import FileBase64 from 'react-file-base64';
import { Button, Comment, Form, Header } from 'semantic-ui-react';
import SweetAlert from 'react-bootstrap-sweetalert';
import Heart from "react-animated-heart";
import { Col, FormGroup, Input, Label, Row, Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavbarText, Container, Modal, ModalHeader, ModalBody, ModalFooter, Badge } from 'reactstrap';
import { FaRegTrashAlt } from 'react-icons/fa';
import axios from "axios";
import { NavLink, Redirect ,useHistory} from "react-router-dom";
import Loaders from '../Loader/Loaders';
import {user,post,token,userId,userName,profileImage,headers} from '../../constants/data'
import "./MyPage.css";
import logo from '../../assests/images/logo.png'

function MyPage() {
    let history = useHistory();
    const [isOpen, setIsopen] = useState(false);
    const [commentSectionView, setCommentSectionView] = useState(false);
    const [postsData, setPostsData] = useState([]);
    const [userComment, setUserComment] = useState('');
    const [userPostImage, setUserPostImage] = useState([]);
    const [userPostTitle, setUserPostTitle] = useState('');
    const [userPostDescription, setUserPostDescription] = useState('');
    const [modal, setModal] = useState(false);
    const [spinner, setSpinner] = useState(false);
    const [swal, setSwal] = useState(false);
    const [postsEmpty, setPostsEmpty] = useState(false);
    const [userProfileImage, setUserProfileImage] = useState([]);
    const [postsMode, setPostsMode] = useState(false);
    const [commentsID, setCommentsID] = useState('');

    useEffect(() => {
        window.scrollTo(0, 0);
        getPrimaryDetails();
    }, []);
    const getPrimaryDetails = async () => {
        setSpinner(true); setPostsEmpty(false);
        const data = {
            userId: userId
        }
        await axios.post(post + "myPosts", data, { headers })
            .then(async dataObj => {
                await setPostsData(dataObj.data.data);
                setSpinner(false); setPostsEmpty(false);
            }).catch(err => {
                setSpinner(false); setPostsEmpty(false); setSwal(true);
            })
    }
    const navbarToggle = () => {
        setIsopen(!isOpen);
    }
    const viewComments = (postId) => {
        setCommentsID(postId); setCommentSectionView(!commentSectionView);
    }
    const comment = async (postId) => {
        setSpinner(true); setPostsEmpty(false);
        if (userComment.length == 0) {
            setSwal(true);
        }
        else {
            var dt = new Date();
            var hours = dt.getHours();
            var AmOrPm = hours >= 12 ? 'pm' : 'am';
            hours = (hours % 12) || 12;
            var minutes = dt.getMinutes();
            var finalTime = hours + ":" + minutes + " " + AmOrPm;
            const data = { userId: userId, postId: postId, comments: userComment, userName: userName, commentedAt: finalTime, profileImage: profileImage }
            await axios.post(post + "addComment", data, { headers }
            ).then(res => {
                setSpinner(false); setPostsEmpty(false); getPrimaryDetails();
            }).catch(err => {
                setSpinner(false); setPostsEmpty(false); setSwal(true); getPrimaryDetails();
            })
        }
    }

    const toggleChange = async (postId, value) => {
        setSpinner(true); setPostsEmpty(false);
        const data = {
            userId: userId,
            postId: postId
        }
        if (value === false) {
            await axios.post(post + "like", data, { headers }).then(res => {
                setSpinner(false); getPrimaryDetails();
            }).catch(err => {
                setSpinner(false); setSwal(true); getPrimaryDetails();
            })
        }
        else {
            await axios.post(post + "unlike", data, { headers }
            ).then(res => {
                setSpinner(false); getPrimaryDetails();
            }).catch(err => {
                setSpinner(false); setSwal(true); getPrimaryDetails();
            })
        }
    }
    const getFiles = async (files) => {
        setUserPostImage(files.base64);
    }
    const modalToggle = async () => {
        setModal(!modal);
    }
    const uploadPosts = async () => {
        setSpinner(true); setModal(false); setPostsEmpty(false);
        const data = { userId: userId, userPostTitle: userPostTitle, userPostDescription: userPostDescription, userPostImage: userPostImage, userName: userName, private: postsMode }
        await axios.post(post + "uploadPost", data, { headers }).then(res => {
            setSpinner(false); getPrimaryDetails();
        }).catch(err => {
            setSpinner(false); setSwal(true); getPrimaryDetails();
        })
    }
    const follow = async (friendsId) => {
        setSpinner(true); setPostsEmpty(false);
        const data = { userId: userId, friendsId: friendsId }
        await axios.post(user + "follow", data, { headers }).then(res => {
            setSpinner(false); getPrimaryDetails();
        }).catch(err => {
            setSpinner(false); setSwal(true);
        })
    }
    const deletePosts = async (postId) => {
        setSpinner(true); setPostsEmpty(false);
        const data = { postId: postId }
        await axios.post(post + "deletePosts", data, { headers }).then(res => {
            setSpinner(false); getPrimaryDetails();
        }).catch(err => {
            setSpinner(false); setSwal(true);
        })
    }
    const logout = async () => {
        localStorage.clear();
        history.push('/login');
    }
    if (!token) {
        return <Redirect to="/login" />
    }
    else {
        return (
            <div>
                <Navbar color="light" light expand="md">
                    <img
                        alt="" src={logo}
                        class="nav-img"
                    />&nbsp;&nbsp;
        <NavbarBrand href="/">Social Media</NavbarBrand>
                    <NavbarToggler onClick={navbarToggle} />
                    <SweetAlert
                        error
                        show={swal}
                        title="Something went wrong, Please logout and login again"
                        onConfirm={() => setSwal(false)}
                    >
                    </SweetAlert>
                    <Collapse isOpen={isOpen} navbar>
                        <Nav className="mr-auto" navbar >
                        </Nav>
                        <NavLink to="/" class="navbar-text"> Posts</NavLink>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br />
                        <NavLink to="/public-posts" class="navbar-text"> Public Posts</NavLink>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br />
                        <NavbarText class="navbar-text" onClick={modalToggle}>Upload Post</NavbarText>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br />
                        <NavLink to="/profile" class="navbar-text"> Profile</NavLink>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br />
                        <NavbarText class="navbar-text" onClick={logout}>Logout</NavbarText>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br />
        </Collapse>
                </Navbar>
                <Container>
                    <Row>
                        <Modal isOpen={modal} toggle={modalToggle} size="lg">
                            <ModalHeader xl={8} lg={12} md={12} toggle={modalToggle}>Upload Post</ModalHeader>
                            <ModalBody>
                                <Form>
                                    <FormGroup row>
                                        <Label sm={4}>
                                            PostTitle
                                        </Label>
                                        <Col sm={8}>
                                            <Input
                                                type="text"
                                                placeholder="Post Title"
                                                value={userPostTitle}
                                                onChange={(event) => setUserPostTitle(event.target.value)}
                                            />
                                        </Col>
                                    </FormGroup>
                                    <FormGroup row>
                                        <Label sm={4}>
                                            Post Description
                                        </Label>
                                        <Col sm={8}>
                                            <Input
                                                type="textarea"
                                                placeholder="Post Description"
                                                value={userPostDescription}
                                                onChange={(event) => setUserPostDescription(event.target.value)}
                                            />
                                        </Col>
                                    </FormGroup>
                                    <FormGroup row>
                                        <Label sm={4}>Post Image</Label>
                                        <Col sm={7}>
                                            <FileBase64 id="profileImage" name="profileImage" required onDone={getFiles.bind(this)} />
                                        </Col>
                                    </FormGroup>
                                    <FormGroup row>
                                        <Label sm={5}>
                                        </Label>
                                        <Col sm={7}>
                                            <Input
                                                type="checkbox"
                                                value={postsMode}
                                                onChange={(e) => setPostsMode(e.target.checked)} />Private Post
                                        </Col>
                                    </FormGroup>
                                </Form>
                            </ModalBody>
                            <ModalFooter>
                                <Button color='success' onClick={uploadPosts}>Save{' '}</Button>
                                <Button color='secondary' onClick={modalToggle}>Cancel</Button>
                            </ModalFooter>
                        </Modal>
                        {spinner === true ?
                            <Loaders />
                            :
                            <Col xl={12} lg={12} md={12} >
                                <div class="gtco-testimonials">
                                    <h2>Posts</h2>
                                    <span>
                                        {postsData.length === 0 ?
                                            <p class="post-data"> No Post found</p>
                                            : <span>
                                                {postsData.map((data, index) => (
                                                    <Row>
                                                        <Col xl={1} lg={1} md={12} ></Col>
                                                        <Col xl={10} lg={10} md={12} >
                                                            <div class="testimonials-div">
                                                                <div class="card text-center">
                                                                    <img class="card-img-top" key={index} src={data.userPostImage} alt="" />
                                                                    <div class="card-body">
                                                                        <h5 key={index} >{data.userName} <br />
                                                                            <span class="card-title"> {data.userPostTitle}</span>
                                                                        </h5>
                                                                        <p class="card-text" >{data.userPostDescription} </p>
                                                                        <Button key={index} onClick={deletePosts.bind(this, data.postId)}><FaRegTrashAlt class="delete" /></Button>
                                                                    </div>
                                                                    <div >
                                                                        <span class="heart-img">
                                                                            <Heart isClick={data.likedUsersId.includes(userId)} onClick={toggleChange.bind(this, data.postId, data.likedUsersId.includes(userId))}  >
                                                                            </Heart> </span> <span class="heart-likes"> {data.likedUsersId.length} Likes</span>
                                                                        <p class="comments-view" key={index} onClick={viewComments.bind(this, data.postId)}><Badge color="secondary">{data.commentsUsersID.length}</Badge> {' '}View Comments</p>
                                                                    </div>
                                                                    {data.postId === commentsID ?
                                                                        <div>
                                                                            {commentSectionView === true ?
                                                                                <Comment.Group>
                                                                                    {data.commentsUsersID.map(
                                                                                        ({ comments, userName, commentedAt, profileImage }) => (
                                                                                            <div>
                                                                                                <Comment style={{ float: 'left' }}>
                                                                                                    <Comment.Avatar key={index} src={profileImage} />
                                                                                                    <Comment.Content >
                                                                                                        <Comment.Author as='a' key={index}>{userName}</Comment.Author>
                                                                                                        <Comment.Metadata>
                                                                                                            <div key={index}>{commentedAt}</div>
                                                                                                        </Comment.Metadata>
                                                                                                        <Comment.Text key={index}>{comments}</Comment.Text>
                                                                                                    </Comment.Content>
                                                                                                </Comment><br /><br /><br />
                                                                                                <br />
                                                                                            </div>
                                                                                        ))}
                                                                                    <div class="comment-box">
                                                                                        <Input type="textarea" name="text" id="exampleText" onChange={(event) => setUserComment(event.target.value)} />
                                                                                        <div class="comment-button">
                                                                                            <Button content='Add Comment' labelPosition='left' icon='edit' primary onClick={comment.bind(this, data.postId)} />
                                                                                        </div>  </div>
                                                                                </Comment.Group>
                                                                                : null}
                                                                        </div>
                                                                        : null}
                                                                </div>
                                                            </div>
                                                        </Col>
                                                    </Row>
                                                ))
                                                }
                                            </span>
                                        }
                                    </span>
                                </div>
                            </Col>
                        }
                    </Row>
                </Container>
            </div>
        )
    }
}

export default MyPage;
