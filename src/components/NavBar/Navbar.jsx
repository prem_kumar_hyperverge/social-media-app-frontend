import React, { useState } from "react";
import { Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavbarText} from 'reactstrap';
import { NavLink, Redirect, useHistory } from "react-router-dom";
function Navbar(props) {
    let history = useHistory();
    const [modal, setModal] = useState(false);
    const logout = async () => {
        localStorage.clear();
        history.push('/login');
    }
  return (
    <Navbar color="light" light expand="md">
                    <img src={logo} class="nav-img"
                    />&nbsp;&nbsp;
        <NavbarBrand href="/">Social Media</NavbarBrand>
                    <NavbarToggler onClick={toggle} />
               
                    <Collapse isOpen={isOpen} navbar>
                        <Nav className="mr-auto" navbar >
                        </Nav>
                        <NavLink to="/public-posts" class="navbar-text"> Public Posts</NavLink>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br />
                        <NavLink to="/my-posts" class="navbar-text"> My Posts</NavLink>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br />
                        <NavbarText class="navbar-text" onClick={()=>  setModal(!modal)}>Upload Post</NavbarText>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br />
                        <NavLink to="/profile" class="navbar-text"> Profile</NavLink>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br />
                        <NavbarText class="navbar-text" onClick={logout}>Logout</NavbarText>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br />
        </Collapse>
                </Navbar>
  )
}
export default Navbar;