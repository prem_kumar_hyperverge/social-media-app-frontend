import React, { useState } from "react";
import { Button, Comment, Form } from 'semantic-ui-react';
import { Col, Input, Row, Badge } from 'reactstrap';
import Heart from "react-animated-heart";
import "./Posts.css";
import { user, post, token, userId, userName, profileImage, headers } from '../../constants/data'
function Posts(props) {
    // console.log(props.uComment);
    const [userComment, setUserComment] = useState('');
    function likes() {
        props.like(props.data.postId,props.data.likedUsersId.includes(userId))
    }
    function viewComments() {
        props.viewComment(props.data.postId)
    }
    function comments() {
        props.comment(props.data.postId)
    }
    function addComment(e){
        console.log(e.target.value)
        props.change(e.target.value);
        // setUserComment(e.target.value);        
    }
    return (
        <Row>
            <Col xl={1} lg={1} md={12} ></Col>
            <Col xl={10} lg={10} md={12} >
                <div class="testimonials-div">
                    <div class="card text-center"><img class="card-img-top" src={props.data.userPostImage} alt="" />
                        <div class="card-body">
                            <h5  >{props.data.userName} <br />
                                <span class="card-title"> {props.data.userPostTitle}</span>
                            </h5>
                            <p class="card-text" >{props.data.userPostDescription} </p>
                        </div>
                        <div >
                            <span class="heart-img">
                                <Heart isClick={props.data.likedUsersId.includes(userId)} onClick={likes}  >

                                </Heart> </span> <span class="heart-likes"> {props.data.likedUsersId.length} Likes</span>
                            <p class="comments-view" onClick={viewComments}><Badge color="secondary">{props.data.commentsUsersID.length}</Badge> {' '}View Comments</p>
                        </div>
                        {props.data.postId === props.cId ?
                            <div>
                                {props.sectionView === true ?
                                    <Comment.Group>
                                        {props.data.commentsUsersID.map(
                                            ({ comments, userName, commentedAt, profileImage }) => (
                                                <div>
                                                    <Comment style={{ float: 'left' }}>
                                                        <Comment.Avatar src={profileImage} />
                                                        <Comment.Content >
                                                            <Comment.Author as='a' >{userName}</Comment.Author>
                                                            <Comment.Metadata>
                                                                <div>{commentedAt}</div>
                                                            </Comment.Metadata>
                                                            <Comment.Text>{comments}</Comment.Text>
                                                        </Comment.Content>
                                                    </Comment><br /><br /><br />
                                                    <br />
                                                </div>
                                            ))}
                                        <div class="comment-box">
                                      
                                            <Input type="textarea" name="text" id="exampleText"   onChange={(e) => addComment(e)} />
                                            <div class="comment-button">
                                                <Button content='Add Comment' labelPosition='left' icon='edit' primary onClick={comments} />
                                            </div>
                                        </div>
                                    </Comment.Group>
                                    : null}
                            </div>
                            : null}
                    </div>
                </div>
            </Col>
        </Row>
    )

}

export default Posts;