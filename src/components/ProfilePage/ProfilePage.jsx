import React,{ useState, useEffect} from "react";
import FileBase64 from 'react-file-base64';
import { Button, CardBody, Col, Form, FormGroup, Input, Label, Row, Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavbarText, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import SweetAlert from 'react-bootstrap-sweetalert';
import { NavLink, Redirect ,useHistory} from "react-router-dom";
import {user,token,userId,headers} from '../../constants/data'
import Loaders from '../Loader/Loaders';
import axios from "axios";
import "./ProfilePage.css";
import logo from '../../assests/images/logo.png'

function ProfilePage() {
    let history = useHistory();
    const [isOpen, setIsopen] = useState(false);
    const [email, setEmail] = useState('');
    const [userName, setUserName] = useState('');
    const [mobileNumber, setMobileNumber] = useState('');
    const [profileImage, setProfileimage] = useState([]);
    const [spinner, setSpinner] = useState(false);
    const [swal, setSwal] = useState(false);
    const [modal, setModal] = useState(false);
    const [friendsList, setFriendsList] = useState([]);
    const [userDetails, setUserDetails] = useState([]);

    useEffect(() => {
        window.scrollTo(0, 0);
        getPrimaryDetails();
    }, []);

    const getPrimaryDetails = async () => {
        setSpinner(true);
        const data = { userId: userId }
        const profileData = await axios.post(user + "profile", data, { headers }).then(async res => {
            console.log(res);
            setEmail(res.data.data.user.email); setMobileNumber(res.data.data.user.mobileNumber);
            setProfileimage(res.data.data.user.profileImage); setUserName(res.data.data.user.userName);
            const data1 = { userId: await res.data.data.user.friendsList }
            await axios.post(user + "getUsername", data1, { headers }).then(res1 => {
                console.log(res1)
                setUserDetails(res1.data.data); setSpinner(false);
            }).catch(err => {
                setSpinner(false); setSwal(true);
            })
            setSpinner(false);
        }).catch(err => {
            setSpinner(false); setSwal(true);
        })            
    }
    const navbarToggle = async () => {
        setIsopen(!isOpen);
    }
    const getFiles = async (files) => {
        setProfileimage(files.base64)
    }
    const modalToggle = async () => {
        setModal(!modal);
    }
    const uploadPost = async () => {
        setSpinner(true); setModal(false);
        const data = { userId: userId, userName: userName, mobileNumber: mobileNumber, profileImage: profileImage }
        await axios.post(user + "update", data, { headers }).then(res => {
            setSpinner(false); getPrimaryDetails();
        }).catch(err => {
            setSpinner(false);  getPrimaryDetails();
        })
    }
    const logout = async () => {
        localStorage.clear();
        history.push('/login');
    }
    if (!token) {
        return <Redirect to="/login" />
    }
    else {
        return (
            <div>
                {spinner === true ?
                    <Loaders />
                    :
                    <div>
                        <Navbar color="light" light expand="md">
                            <SweetAlert
                                error
                                show={swal}
                                title="Something went wrong, Please logout and login again"
                                onConfirm={() => setSwal(false)}
                            >
                            </SweetAlert>
                            <img
                                alt=""
                                src={logo}
                                class="nav-img"
                            />&nbsp;&nbsp;
                                <NavbarBrand href="/">Social Media</NavbarBrand>
                            <NavbarToggler onClick={navbarToggle} />
                            <Collapse isOpen={isOpen} navbar>
                                <Nav className="mr-auto" navbar >
                                </Nav>
                                <NavLink to="/" class="navbar-text"> Posts</NavLink>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br />
                                <NavbarText class="navbar-text" onClick={logout}>Logout</NavbarText>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br />
                                </Collapse>
                        </Navbar>
                        <Col xl={12} lg={12} md={12} >
                            <div class="gtco-testimonials">
                                <h2>My Profile</h2>
                                <Row>
                                    <Col xl={3} lg={12} md={12} >
                                    </Col>
                                    <Col xl={6} lg={12} md={12} >

                                        <div class="card text-center"><img class="card-img-profile" src={profileImage} alt="" />
                                            <div class="card-body">
                                                <h5>{userName} <br />
                                                </h5>
                                                <CardBody>
                                                    <Form>
                                                        <FormGroup row>
                                                            <Label sm={4}>
                                                                Email
                                                                    </Label>
                                                            <Col sm={8}>
                                                                <Input
                                                                    disabled
                                                                    placeholder={email}
                                                                />
                                                            </Col>
                                                        </FormGroup>
                                                        <FormGroup row>
                                                            <Label sm={4}>
                                                                Name
                                                                    </Label>
                                                            <Col sm={8}>
                                                                <Input
                                                                    disabled
                                                                    placeholder={userName}
                                                                />
                                                            </Col>
                                                        </FormGroup>
                                                        <FormGroup row>
                                                            <Label sm={4}>
                                                                Mobile Number
                                                                     </Label>
                                                            <Col sm={8}>
                                                                <Input
                                                                    disabled
                                                                    placeholder={mobileNumber}
                                                                />
                                                            </Col>
                                                        </FormGroup>
                                                    </Form>
                                                    <center>
                                                        <Button class="modal" onClick={modalToggle}> Edit</Button>
                                                    </center>
                                                </CardBody>
                                                <h5 clas="follow-name">Followers <br /></h5>
                                                <Row>
                                                    {userDetails.map((data, index) => (
                                                        <Col xl={4} lg={4} md={6} sm={6} >
                                                            <div class="follow-div">
                                                                <div class="card text-center" ><img class="card-img-tops" key={index} src={data.profileImage} alt="" />
                                                                    <div class="card-body">
                                                                        <h5 key={index} >{data.userName} <br /></h5>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </Col>
                                                    ))}
                                                </Row>
                                                <Modal isOpen={modal} toggle={modalToggle} size="lg">
                                                    <ModalHeader xl={8} lg={12} md={12} toggle={modalToggle}>Edit</ModalHeader>
                                                    <ModalBody>
                                                        <Form>
                                                            <FormGroup row>
                                                                <Label sm={4}>
                                                                    Name
                                                                        </Label>
                                                                <Col sm={8}>
                                                                    <Input
                                                                        placeholder={userName}
                                                                        value={userName}
                                                                        onChange={(event) => setUserName(event.target.value)}
                                                                    />
                                                                </Col>
                                                            </FormGroup>
                                                            <FormGroup row>
                                                                <Label sm={4}>
                                                                    Mobile Number
                                                                        </Label>
                                                                <Col sm={8}>
                                                                    <Input
                                                                        value={mobileNumber}
                                                                        onChange={(event) => setMobileNumber(event.target.value)}
                                                                    />
                                                                </Col>
                                                            </FormGroup>
                                                            <FormGroup row>
                                                                <Label sm={4}>Profile Picture</Label>&nbsp;&nbsp;&nbsp;&nbsp;
                                                                            <FileBase64 sm={8} id="profileImage" name="profileImage" required onDone={getFiles.bind(this)} />
                                                            </FormGroup>
                                                        </Form>
                                                    </ModalBody>
                                                    <ModalFooter>
                                                        <Button color='success' onClick={uploadPost}>Save{' '}</Button>
                                                        <Button color='secondary' onClick={modalToggle}>Cancel</Button>
                                                    </ModalFooter>
                                                </Modal>
                                            </div>
                                        </div>
                                    </Col>
                                </Row>
                            </div>
                        </Col>
                    </div>
                }
            </div>
        )

    }
}
export default ProfilePage;
