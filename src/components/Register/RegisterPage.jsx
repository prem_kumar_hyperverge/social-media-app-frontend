import React, { useState } from "react";
import axios from 'axios';
import { NavLink, Redirect ,useHistory, Link} from "react-router-dom";
import FileBase64 from 'react-file-base64';
import { Button, CardBody, Col, FormGroup, Input, Label, Row, Container } from 'reactstrap';
import SweetAlert from 'react-bootstrap-sweetalert';
import Loaders from '../Loader/Loaders';
import "./register.css";
import { user } from '../../constants/data'

function Registerpage() {
  let history = useHistory();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [username, setUsername] = useState('');
  const [mobilenumber, setmobilenumber] = useState('');
  const [profileimage, setProfileimage] = useState('');
  const [spinner, setSpinner] = useState(false);
  const [swal, setSwal] = useState(false);

  const register = async () => {
    setSpinner(true);
    await axios.post(user + 'register', {
      email: email,
      password: password,
      userName: username,
      mobileNumber: mobilenumber,
      profileImage: profileimage
    }).then(response => {
      console.log(response)
      setSpinner(false);
      window.location.href="/login";
    }).catch(err => {
      console.log(err)
      setSpinner(false); setSwal(true);
    })
  }
  const getFiles = async (files) => {
    setProfileimage(files.base64);
  }
  return (
    <div>
      <Container>
        {spinner === true ?
          <Loaders />
          :
          <Row>
            <Col xl={5} lg={8} md={8} style={{
              position: 'absolute', left: '50%', top: '50%',
              transform: 'translate(-50%, -50%)', boxShadow: '0px 0px 15px rgba(0,0,0,0.15)'
            }}>
              <center>
                <h3 style={{ marginTop: '2%' }}>Sign Up</h3></center>
              <CardBody>
                <FormGroup>
                  <Label >Email</Label>
                  <Input type="email" placeholder="john@gmail.com" onChange={(event) => setEmail(event.target.value)} style={{ backgroundColor: '#F0F5FB' }} />
                </FormGroup>
                <FormGroup>
                  <Label >User Name</Label>
                  <Input type="text" placeholder="Johnson" onChange={(event) => setUsername(event.target.value)} style={{ backgroundColor: '#F0F5FB' }} />
                </FormGroup>
                <FormGroup>
                  <Label >Password</Label>
                  <Input type="password" placeholder="password" onChange={(event) => setPassword(event.target.value)} style={{ backgroundColor: '#F0F5FB' }} />
                </FormGroup>
                <FormGroup>
                  <Label >Mobile Number</Label>
                  <Input type="text" placeholder="9632587410" onChange={(event) => setmobilenumber(event.target.value)} style={{ backgroundColor: '#F0F5FB' }} />
                </FormGroup>
                <FormGroup>
                  <Label >Profile Picture</Label>&nbsp;&nbsp;&nbsp;&nbsp;
        <FileBase64 id="profileImage" name="profileImage" required onDone={getFiles.bind(this)} />
                </FormGroup>
                <center> <Button color="success" onClick={register}>Register</Button></center>
                <p className="text-right">
                  Already User <Link to='/login' >Login </Link> 
                </p>
              </CardBody>
              <SweetAlert
                error
                show={swal}
                title="Please enter valid details"
                onConfirm={() => setSwal(false)}
              >
              </SweetAlert>
            </Col>
          </Row>
        }
      </Container>
    </div>
  )
}
export default Registerpage;
