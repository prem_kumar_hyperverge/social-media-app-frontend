export const user = "http://localhost:3333/auth/";
export const post = "http://localhost:3333/post/";
export const userName = localStorage.getItem("userName");
export const token = localStorage.getItem("token_social_media");
export const userId = localStorage.getItem("userId");
export const profileImage = localStorage.getItem("profileImage");
export const headers =
{
    'content-type': 'application/json',
    'x-access-token': token
}